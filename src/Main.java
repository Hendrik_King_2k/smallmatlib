import abstract_stuff.MatrixFunction;
import basic_stuff.Matrix;
import basic_stuff.Operations;

/**
 * Created by Hendrik on 15.09.2018.
 */
public class Main {

    public static void main(String[] args){

        MatrixFunction nonlin_deriv = new MatrixFunction() {
            @Override
            public float compute(float... in) {
                return in[0] * (1f - in[0]);
            }
        };

        MatrixFunction nonlin = new MatrixFunction() {
            @Override
            public float compute(float... in) {
                return (float) (1f / (1f + Math.exp(-in[0])));
            }
        };

        //TODO input and output vals

        Matrix X = new Matrix(4, 3, true);

        X.setValue(new float[][] {
                {0, 0, 1},
                {0, 1, 1},
                {1, 0, 1},
                {1, 1, 1}
        });

        Matrix Y = new Matrix(4, 1, true);

        Y.setValue(new float[][] {
                {0},
                {1},
                {1},
                {0}
        });


        Matrix syn0 = Operations.add(Operations.mul(new Matrix(3, 40, true), 2f), -1);
        Matrix syn1 = Operations.add(Operations.mul(new Matrix(40, 40, true), 2f), -1);
        Matrix syn2 = Operations.add(Operations.mul(new Matrix(40, 20, true), 2f), -1);
        Matrix syn3 = Operations.add(Operations.mul(new Matrix(20, 1, true), 2f), -1);

        //Used in loop

        Matrix l0, l1, l2, l3, l4 = null;

        Matrix l4_err, l3_err, l2_err, l1_err;

        Matrix l4_delta, l3_delta, l2_delta, l1_delta;

        for (int j = 0; j < 60000; j++){

            l0 = X;
            l1 = nonlin.compute(l0.dot(syn0));
            l2 = nonlin.compute(l1.dot(syn1));
            l3 = nonlin.compute(l2.dot(syn2));
            l4 = nonlin.compute(l3.dot(syn3));

            l4_err = Y.sub(l4);

            if(j % 10000 == 0){
                System.out.println("Error "+Math.abs(l4_err.mean()));

            }

            l4_delta = l4_err.mul(nonlin_deriv.compute(l4));

            l3_err = l4_delta.dot(syn3.transpose());
            l3_delta = l3_err.mul(nonlin_deriv.compute(l3));

            l2_err = l3_delta.dot(syn2.transpose());
            l2_delta = l2_err.mul(nonlin_deriv.compute(l2));

            l1_err = l2_delta.dot(syn1.transpose());
            l1_delta = l1_err.mul(nonlin_deriv.compute(l1));

            //update weights

            syn3 = syn3.add(l3.transpose().dot(l4_delta));
            syn2 = syn2.add(l2.transpose().dot(l3_delta));
            syn1 = syn1.add(l1.transpose().dot(l2_delta));
            syn0 = syn0.add(l0.transpose().dot(l1_delta));

        }

        System.out.println("Output after Training: ");

        System.out.println(l4);
    }

}
