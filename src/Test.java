import abstract_stuff.MatrixFunction;
import basic_stuff.Matrix;

/**
 * Created by Hendrik on 15.09.2018.
 */
public class Test {

    public static void main(String[] args) {
        Matrix matrix = new Matrix(3, 4, false);
        Matrix matrix2 = new Matrix(4, 3, false);

        MatrixFunction nonlin_deriv = new MatrixFunction() {
            @Override
            public float compute(float... in) {
                return in[0] * (1f - in[0]);
            }
        };

        MatrixFunction nonlin = new MatrixFunction() {
            @Override
            public float compute(float... in) {
                return (float) (1f / (1f + Math.exp(-in[0])));
            }
        };

        System.out.println(nonlin_deriv.compute(0));
        System.out.println(nonlin_deriv.compute(1));
        System.out.println(nonlin_deriv.compute(0.5f));
        System.out.println(nonlin_deriv.compute(0.25f));
        System.out.println(nonlin_deriv.compute(0.75f));
    }

}
